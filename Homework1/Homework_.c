#include <stdio.h>
#include <math.h>														// I took a lot of variables because it was easier for me to divide whole equation into parts
int main(){
    int i;																//declaring variables with type integer				
    double o, t, p, k, g, y, H, InitialNumber, StoppingValue, x, steps;		//declaring variables with type double

    printf("Input initial number\n");									//program prints "Input initial number"
     scanf("%lg",&InitialNumber);										//user inputs initial number
    printf("Input stopping value\n");									//program prints "Input stopping value"
     scanf("%lg",&StoppingValue);										//user inputs stopping Value
    printf("Input number of steps  (steps > 1, steps <= 15)\n");		//program prints "Input number of steps  (steps > 1, steps <= 15)"
     scanf("%lg",&steps);												//user inputs number of steps
    if(steps > 15){														//loop:if number of steps will be more than 15 the program prints Error! and stops
        printf("Error!");
        return 0;
    }
    else if(steps <= 0){
		printf("Error!");
		return 0;
	}
    H = (steps - InitialNumber) / (StoppingValue - 1);					//equation for H
    printf("x\ty=f(x)");												//program prints x and y=f(x)
       for(i = 0; i <= (steps - 1); i++){								//loop:i = 0, when i less than number of steps minus 1 or equal to this, next step will be with i +=1
        x = InitialNumber + (i * H);									//equation for x
        t = 1 + (x * x);												//equation for t
        p = 4 -(x * x);													//equation for p
        o = (t / p);													//equation for o
        k = sqrt(o);													//equation for t
        g = 6 * x * x - k;												//equation for g
        y = g / (8 -(x * x * x));										//equation for y
        if (p < 0){														//in case p less than 0 program prints x and Infinity and continues
			printf("\n%.4lg\tInfinity", x);
		}
        else if (p == 0){												//in case p will be equal to 0 program prints x and Complex number and continues
				printf("\n%.4lg\tComplex number", x);
		}
       else printf("\n%.4lg\t%.3lg",x,y); 								//program prints x and function y
    }
    return 0;															//program stops
}
